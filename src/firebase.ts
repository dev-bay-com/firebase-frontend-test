import * as firebase from 'firebase/app';
import "firebase/auth";

const firebaseConfig = {
  apiKey: "YOUR-APP-KEY",
  authDomain: "YOUR-APP-URL",
  databaseURL: "YOUR-APP-URL",
  projectId: "YOUR-PROJECT-ID",
  storageBucket: "YOUR-APP-URL",
  messagingSenderId: "YOUR-CODE",
  appId: "YOUR-CODE"
}

firebase.initializeApp(firebaseConfig);

export default {
  firebase
}